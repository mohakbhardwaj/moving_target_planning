#include <exception>
#include "planner_main.hpp"



namespace mtp{


std::vector<int> ConfigFileParser::stringtoInt(std::string Input){
	int i;
	std::vector<int> Output;
	std::stringstream ss(Input);
	while (ss >> i){
		Output.push_back(i);
		if (ss.peek() == ',')
			ss.ignore();
	}
	return Output;
}


CompleteDefinition ConfigFileParser::ReadFile(){
    std::string line;
	config_.open(configFile_.c_str());
	char flag;
	std::vector<R2TimeStatePtr> trajectory;
	std::vector<std::vector<int> > costmap;
	int gridDim;
	int timestep = 0;
	R2TimeStatePtr start = std::make_shared<R2TimeState>();
	while (getline(config_, line)){
		if (line[0] == 'B'){
			flag = 'B';
			continue;
		}
		else if (line[0] == 'T'){
			flag = 'T';
			continue;
		}
		else if (line[0] == 'N'){
			flag = 'N';
			continue;
		}
		else if (line[0] == 'R'){
			flag = 'R';
			continue;
		}
	    if (flag == 'B'){
			costmap.push_back(stringtoInt(line));
		}
		else if (flag == 'T'){
			std::vector<int> temp = stringtoInt(line);
			R2TimeStatePtr tempState = std::make_shared<R2TimeState>(temp[0], temp[1], timestep);
			++timestep;
			trajectory.push_back(tempState);
		}
		else if (flag == 'N'){
			flag = 'X';
			gridDim = stoi(line);
		}
		else if (flag == 'R'){
			flag = 'X';
			std::vector<int> startCoords = stringtoInt(line);
			start.get()->y = startCoords[0];
			start.get()->x = startCoords[1];
			start.get()->t = 0;
		}

	}

	//Pack into CompleteDefinition structure
		PlanningProblem problem(start, trajectory);
		CompleteDefinition cd_;
		cd_.gridSize = gridDim;
		cd_.costMap = costmap;
		cd_.problem = problem;
		cd_.timeBound = timestep;
		std::cout << "Loaded \n" << std::endl;
		return cd_;
 }

bool PlannerMain::initialize(const std::string planner_name){
	std::cout << "[INFO] Initializing planners\n";
	std::string planner_a, planner_b;
	if(planner_name.compare("planner_1") == 0){
		planner_a = "djikstra_planner";
		planner_b = "backward_astar_planner";
	}
	else if(planner_name.compare("planner_2") == 0){
		planner_a = "djikstra_planner";
		planner_b = "backward_weighted_astar_planner";
	}

	if(BasePlannerDatabase.count(planner_a) == 0 || BasePlannerDatabase.count(planner_b) == 0){
			err_str_ = "[ERR] Base planner not in database";
			return false;
		}
	else{

		base_planner_a = BasePlannerDatabase[planner_a];
		base_planner_b = BasePlannerDatabase[planner_b];
		base_planner_a->Initialize(100, true, true, dummyHeuristic, GridCost);
		base_planner_b->Initialize(100, true, true, dummyHeuristic, GridCost);
	}
	

	return true;
  }

bool PlannerMain::Plan(const PlanningProblem& problem, PlanningSolution& solution) const{


	base_planner_a->Refresh();
	base_planner_b->Refresh();
	//First run planner_a and use the costmap obtained from it as heurstic for planner_b
	std::cout << "[INFO] Running forward Djikstra search for informed heuristics\n";
	if(base_planner_a->Solve(problem, solution)){
		std::vector<std::vector<int> > heuristic_map;
		std::cout << "[INFO] Retrieving informed heuristcs from forward search\n";
		bool gotCosts = base_planner_a->getCachedCosts(heuristic_map);
		if(gotCosts){
			std::cout << "[INFO] Successfully retrieved heuristics\n";
		}
		else{
			// err_str_ = "Failed to initialize heuristics\n";
			return false;}

		std::cout << "[INFO] Setting heuristics for backward planner" << std::endl;
		base_planner_b->setHeuristicCache(heuristic_map);
		std::cout << "[INFO] Running backward searh for solution path\n";
		if(base_planner_b->Solve(problem, solution)){
			std::cout << "[INFO] Planner Success" << std::endl;
		}
	}
	else{	
		// err_str_ = "Base Planner Failed to provide solution \n";
		return false;
	}
	return true;		
}

//Utility Functions for printing states
void PrintStatePtr(R2TimeStatePtr sptr){
		std::cout << "[" << sptr.get()->y << ", " << sptr.get()->x << ", t: " << sptr.get()->t << "]" << std::endl;
	}

void PrintStatePtrVector(std::vector<R2TimeStatePtr> sptrVector){
		for(size_t i = 0; i < sptrVector.size(); ++i){
			PrintStatePtr(sptrVector[i]);
		}
	}
void PrintStatePtrSet(std::set<R2TimeStatePtr> sptrSet){
		for(auto iter: sptrSet){
			PrintStatePtr(iter);
		}
	}


void PrintPlanningProblem(const PlanningProblem p){
	std::cout << "Start "; PrintStatePtr(p.start);
	std::cout << "Goals:\n";PrintStatePtrVector(p.goals);		
}

void PrintCompleteDefinition(const CompleteDefinition& cd){
	std::cout << "Grid Size: " << cd.gridSize << std::endl;
	std::cout << "Cost Map: " << std::endl;
	for(auto iter1:cd.costMap){
		for (auto iter2:iter1){
			std::cout << iter2 << " ";
		}
		std::cout << "\n";
	}
	std::cout << "Planning Problem: " << std::endl;
	PrintPlanningProblem(cd.problem);
	}


} //namespace mtp
