# README #

Planner for a moving target in a known environment with known trajectory. 

The following steps are required to run the planners on a new computer

1. cd build
2. Delete all files in build directory (if any)
3. From inside build directory run the following two commands
		 - cmake ..
		 - make
4. This will create two executables for moving-target-a and moving-target-b
5. The executables can be run in the following way:
	./moving-target-a <problem-file> <print-solution>

	- The value of problem-file argument must be problem-0 or problem-1 not full file path 

	- The problem files should be placed in the resources folder in main directory with names problem-0.txt or problem-1.txt (example problem files are included in resources folder)

	- If print-solution is 1, the complete solution will be printed on the screen and saved in file as well, else complete path will be saved in file only.
	
	- The solution will be saved in resources folder wth name moving-target-a-solution.txt or moving-target-b-solution.txt
