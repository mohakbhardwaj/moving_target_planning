#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_BASE_PROBLEM_PROCESSOR_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_BASE_PROBLEM_PROCESSOR_H_

#include <vector>
#include <memory>
#include <string>
#include <set>
#include <cstddef>
#include "planners/search_planning_common.hpp"
#include "planning_common.hpp"

namespace mtp{


	class BaseProblemProcessor{
	public:
		BaseProblemProcessor(){}

		static void unPackPlanningProblem(const PlanningProblem &problem, NodePtr &startNode, std::set<NodePtr>& goalNodes);

		static void PackIntoPlanningSolution(PlanningSolution& solution);


	};

} //namespace mtp








#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_BASE_PROBLEM_PROCESSOR_H_