#include "planners/Djikstra2DForwardPlanner.hpp"


namespace mtp{
	bool Djikstra2DPlanner::Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr&)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost){

	planTime_ = planTime_;
	ignore_ = ignore;
	cost_ = cost;
	savePathCosts_ = savePathCosts;
	}

	void Djikstra2DPlanner::Refresh(){
		open = std::priority_queue<NodePtr, std::vector<NodePtr>, NodeCmp>();
		closed.clear();
	}

	void Djikstra2DPlanner::getSuccessorNodes(const NodePtr& s, std::vector<NodePtr>& succNodes){

		//Problem specifies four connected grid only

		R2TimeStatePtr curr_state = s.get()->state;
		
		for(auto primitive : primitives){

			int y = curr_state.get()->y + primitive[0];
			int x = curr_state.get()->x + primitive[1];
			if(r2_tspace_.get()->isValid(y, x, 0)){
				R2TimeStatePtr succ_state = std::make_shared<R2TimeState>(y, x, 0);
				int succ_gVal = s.get()->gValue + costMap[y][x]; //+ cost_(succ_state, env_);		
				if(isClosed(succ_state)){
					continue;
				}
				else{
					NodePtr succ_node = std::make_shared<Node>(succ_state, s, succ_gVal, succ_gVal);
					addToClosed(succ_node);
					if(savePathCosts_){
					int r = succ_node.get()->state.get()->y;
					int c = succ_node.get()->state.get()->x;
					int costVal = succ_node.get()->gValue;
					costCache[r][c] = costVal;
					}
					succNodes.push_back(succ_node);
				}
						
				}
			}
		}

	bool Djikstra2DPlanner::inCurrentGoalSet(const R2TimeStatePtr& state, const std::unordered_set<int>& curr_goal_set) const{

		if(curr_goal_set.count(state.get()->toId(gridSize)) == 0){
			return false;
		}
		return true;
	}

	bool Djikstra2DPlanner::isClosed(const R2TimeStatePtr& n1) const{

		if(closed.count(n1.get()->toId(gridSize)) == 0){
			return false;
		}
		return true;
	}

	void Djikstra2DPlanner::addToClosed(const NodePtr& n1){
		closed.insert(n1.get()->state.get()->toId(gridSize));
	}

	void Djikstra2DPlanner::popFromGoalSet(const R2TimeStatePtr& s, std::unordered_set<int>& curr_goal_set){

		if(inCurrentGoalSet(s, curr_goal_set)){
			curr_goal_set.erase(s.get()->toId(gridSize));
		} 
	}


	
	bool Djikstra2DPlanner::Solve(const PlanningProblem& problem, PlanningSolution& solution){
		

		auto start_time = std::chrono::high_resolution_clock::now();
		//Set the start Node
		std::vector<R2TimeStatePtr> curr_goal_set = problem.goals;
		std::unordered_set<int> goal_set;
		//Prune out the goal points that can never be reached
		std::vector<R2TimeStatePtr> pruned_goal_set = PruneGoals(problem.start, curr_goal_set);
		
		//Note: Some goal points are repeated and might cause hash collisions
		//but since we are doing Djikstra in 2D (ignoring time), it is alright to 
		//allow that.
		for(auto iter: pruned_goal_set){
			auto ins = goal_set.insert(iter.get()->toId(gridSize));
		}
		
		//Parent of start must be NULL
		NodePtr startParent;
		NodePtr start = std::make_shared<Node>(problem.start, startParent, 0, 0);

		open.push(start);
		addToClosed(start);
		int i = 0;
		
		std::vector<NodePtr> succNodes;
		NodePtr curr_node = std::make_shared<Node>();

		while(!open.empty()){
			++i;
		if(open.size() > 5000000){ //[TODO:Experiment with this and planning time]
			err_str_ = "Timeout";
			break;
		}		
		// NodePtr curr_node = std::make_shared<Node>();
		curr_node = open.top();
		open.pop();
		succNodes.clear();
		getSuccessorNodes(curr_node, succNodes);
		for(auto iter: succNodes){
			open.push(iter);
		}   
	}
	if(savePathCosts_){
		costCacheInitialized = true;
	}
	
	auto end_time = std::chrono::high_resolution_clock::now();
	std::cout << "[INFO] Forward Planner took: " << std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count() << " seconds\n";
	return true;
	}
	


}