from RRTTree import RRTTree
from random import random
from SimpleEnvironment import SimpleEnvironment
import collections
import numpy as np


compare = lambda x, y: collections.Counter(x) == collections.Counter(y)


class RRTPlanner:

    def __init__(self, state, N, start_config):
        self.planning_env = SimpleEnvironment(state, N)
        self.tree = RRTTree(self.planning_env, np.array(start_config))
        self.start_config = start_config

    def Plan(self, goal_config, steps, prob_goal):
        # TODO: Here you will implement the rrt planner
        #  The return path should be an array
        #  of dimension k x n where k is the number of waypoints
        #  and n is the dimension of the robots configuration space
        while True:
            if random() < prob_goal:
                target_config = goal_config
            else:
                target_config = self.planning_env.GenerateRandomConfiguration()
            print 
            n_ind, n_vertex = self.tree.GetNearestVertex(target_config)

            if compare(n_vertex, target_config):
                continue

            e_vertex = self.planning_env.Extend(n_vertex, target_config)

            # print "Vertex", n_vertex, e_vertex

            if e_vertex is None:
                pass
            else:
                if compare(e_vertex, goal_config):
                    e_vid = n_ind
                    break
                else:
                    steps = min(10, (self.planning_env.ComputeDistance(n_vertex, e_vertex) + 1) / 10)
                    
                    y = [int(i) for i in np.linspace(n_vertex[0], e_vertex[0], steps)]
                    x = [int(i) for i in np.linspace(n_vertex[1], e_vertex[1], steps)]
                    configs = zip(y, x)
                    for i in configs:
                        e_vid = self.tree.AddVertex(np.array(i))
                        self.tree.AddEdge(n_ind, e_vid)
                        n_ind = e_vid

        plan = [np.array(self.start_config)]


        #traverse backwards
        while e_vid != 0:
            #insert at front of the list
            plan.insert(1, np.array(self.tree.vertices[e_vid]))
            e_vid = self.tree.edges[e_vid]

        plan.append(np.array(goal_config))

        plan = self.planning_env.ShortenPath(plan)
        return plan
