#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_PLANNING_COMMON_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_PLANNING_COMMON_H_

#include <memory>
#include <vector>
#include <functional>
#include <string>
#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"

namespace mtp{

	
	struct Node{
		R2TimeStatePtr state;
		std::shared_ptr<Node> parent;
		double gValue, fValue;    //Change to double if want to try Euclidean Heuristic
		double getField(const std::string field){
			if(field.compare("g")){
				return gValue;
			}
			return fValue;
		}
	};
	typdef std::shared_ptr<Node> NodePtr;

	//Functor Class which can be used to compare nodes
	class NodeCmp{
	public:
		NodeCmp(std::string field)
		:field_{field}
		bool operator()(const NodePtr n1, const NodePtr n2){
			n1.get()->getField(field_) > n2.get()->getField(field_);
		}
	private:
		std::string field_;

	};

	

} //namespace mtp



#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_PLANNING_COMMON_H_