#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_HEURISTICS_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_HEURISTICS_H_

#include <memory>
#include <vector>
#include <set>

#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"
#include "planners/search_planning_common.hpp"


namespace mtp{


	int ManhattanHeuristic(const R2TimeStatePtr& s1, const R2TimeStatePtr& s2, const R2TimeStateSpacePtr& r2_tspace_);

	int ManhattanTimeHeuristic(const R2TimeStatePtr& s1, const R2TimeStatePtr& s2, const R2TimeStateSpacePtr& r2_tspace_, bool timeFlag);

	int ManhattanTimeHeuristicSet(const R2TimeStatePtr& s1, const std::vector<R2TimeStatePtr>& sset, const R2TimeStateSpacePtr& r2_tspace_, const EnvPtr& env);

	int dummyHeuristic(const R2TimeStatePtr& s);
} //namespace mtp

#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_HEURISTICS_H_