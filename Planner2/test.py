#!/usr/bin/env python
import numpy

def ComputeDistance(start_config, end_config):

    return abs(start_config[0] - end_config[0]) + abs(start_config[1] - end_config[1])


def Extend(start_config, end_config):
    
	# dist = .ComputeDistance(start_config, end_config)
	number_of_steps = int(ComputeDistance(start_config, end_config) + 1) / 3#int(np.floor((dist*10)))
    # y = [int(numpy.floor(i)) for i in numpy.linspace(start_config[0], end_config[0], number_of_steps)]
    # x = [int(numpy.floor(i)) for i in numpy.linspace(start_config[1], end_config[1], number_of_steps)]
    # r = range(0, number_of_steps + 1)
	config_to_return = None
    #Move in y first
	for i in xrange(1,number_of_steps + 1):
		direction = numpy.sign(end_config[0] - start_config[0])
		temp_config = numpy.array([start_config[0] + direction*i, start_config[1]])
		config_to_return = temp_config
	start_config = config_to_return
        #Move in x now
        for i in xrange(1, number_of_steps + 1):
            direction = numpy.sign(end_config[1] - start_config[1])
            temp_config = numpy.array([start_config[0], start_config[1] + direction*i])
            config_to_return = temp_config
	return config_to_return



start_config = [5,5]
end_config = [0,0]

print Extend(start_config, end_config)
