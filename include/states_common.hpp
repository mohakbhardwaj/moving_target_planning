#ifndef MOVING_TARGET_PLANNER_INCLUDE_STATES_COMMON_H_
#define MOVING_TARGET_PLANNER_INCLUDE_STATES_COMMON_H_

#include <stdlib.h>

namespace mtp{
	namespace states_common{

		struct R2TimeState{

			R2TimeState()
			: x{0}
			, y{0}
			, t{0}

			R2TimeState(int y, int x, int t)
			: x{x}
			, y{y}
			, t{t}
			{}

			int x,y,t;
			// void setX(const double x_){x = x_;}
			// void setY(const double y_){y = y_;}
			// void setT(const int t_){t = t_;}
			// double getX
		};

		struct R2Bounds{
			R2Bounds()
			{x_lower = 0;
			 y_lower = 0;
			 x_upper = 0;
			 y_lower = 0;}

			R2Bounds(int y_l, int y_u, int x_l, int x_u)
			: x_lower{x_l}
			, x_upper{x_u}
			, y_lower{y_l}
			, y_upper{y_u}
			{}
			int x_lower, x_upper, y_lower, y_upper;
		};

		
		class R2TimeStateSpace{

		public:
			R2TimeStateSpace(){}
			void setBounds(int y_l, int y_u, int x_l, int x_u){
				bounds.y_lower = y_l;
				bounds.y_upper = y_u;
				bounds.x_lower = x_l;
				bounds.x_upper = x_u;
			}
			bool isValid(const R2TimeState state) const{
				if(state.x < bounds.x_lower || state.x > bounds.x_upper
					|| state.y < bounds.y_lower || state.y > bounds.y_upper){
					return false;
				}
				return true;
			}
			
			R2Bounds getBounds(){return bounds;}
			
			int getManhattan(const R2TimeState state_1, const R2TimeState state_2) const{
				return abs(state_1.x - state_2.x) + abs(state_1.y - state_2.y);
			}

		private:
			R2Bounds bounds;

		};

	} //namespace states_common
} //namespace mtp




#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNING_STATE_H_












