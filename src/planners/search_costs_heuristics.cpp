#include "planners/search_heuristics.hpp"

namespace mtp{

	int ManhattanHeuristic(const R2TimeStatePtr& s1, const R2TimeStatePtr& s2, const R2TimeStateSpacePtr& r2_tspace_){
	//Plain old Manhattan Heuristic
	return r2_tspace_.get()->getManhattan(s1, s2);
	}


	int ManhattanTimeHeuristic(const R2TimeStatePtr& s1, const R2TimeStatePtr& s2, const R2TimeStateSpacePtr& r2_tspace_, bool timeFlag){
	//Manhattan Heuristic taking time to go into account
	int timeDiff = s2.get()->t - s1.get()->t;
	timeFlag = true;
	timeDiff = timeDiff > 0? timeDiff:0;
	if(timeDiff == 0){
		timeFlag = false;
	}
	return r2_tspace_.get()->getManhattan(s1, s2) + timeDiff;
	}
    
    int ManhattanTimeHeuristicSet(const R2TimeStatePtr& s1, const std::vector<R2TimeStatePtr>& sset, const R2TimeStateSpacePtr& r2_tspace_, const EnvPtr& env){
    	//[TODO: Compute Efficiently. Explore storing goals as heap maybe]
    	bool timeflag_0;
    	int minH = ManhattanTimeHeuristic(s1, sset[0], r2_tspace_, timeflag_0) ;
    	for(auto iter: sset){
    		bool timeflag_i = true;
    		int h = ManhattanTimeHeuristic(s1, iter, r2_tspace_, timeflag_i);
    		if(!timeflag_i){
    			continue;
    		}
    		if(h < minH){
    			minH = h;
    		}
    	}   	
    	return minH;
    }

    int dummyHeuristic(const R2TimeStatePtr& s){
    	return 0;
    }
	//Costs Implmented
	int GridCost(const R2TimeStatePtr& s, const EnvPtr& env){
		return env->getCost(s);

	}

	int GridAndTimeCost(const R2TimeStatePtr& s, const EnvPtr& env){
		return env->getCost(s) + 10*s.get()->t;
	}

} //namespace mtp