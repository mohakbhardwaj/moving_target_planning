#ifndef MOVING_TARGET_PLANNER_INCLUDE_REPRESENTATION_INTERFACE_H_
#define MOVING_TARGET_PLANNER_INCLUDE_REPRESENTATION_INTERFACE_H_
//Related Headers
//C Headers
//C++ Headers
#include <iostream>
#include <vector>
#include <sstream>
#include <memory>
#include <fstream>
#include <exception>
//Other libraries
//Project Headers
#include "planning_common.h"

namespace mtp{
	
	namespace rep_interface{

		class RepresentationInterface{
		public:
			RepresentationInterface(std::string configfile);
			bool inCollision(const R2TimeState& state) const;
			double getCost(const R2TimeState& state) const;
			bool ReadInputFile();
			void getCostmap(std::vector<std::vector> >& c);

		private:
			std::ifstream env_file;
			std::vector<std::vector<int> > costMap;
			std::string configfile;

		};

	} //namespace ri

} //namespace mtp

#endif