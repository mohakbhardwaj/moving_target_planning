#ifndef MOVING_TARGET_PLANNER_INCLUDE_STATES_COMMON_H_
#define MOVING_TARGET_PLANNER_INCLUDE_STATES_COMMON_H_
/*Header file that defines custom states and state spaces required for the planning problem*/
#include <stdlib.h>
#include <memory>
#include <string>

namespace mtp{
	
		struct R2TimeState{
		R2TimeState()
		: x{0}
		, y{0}
		, t{0}
		{}
		R2TimeState(int y, int x, int t)
		: x{x}
		, y{y}
		, t{t}
		{}
		std::string toString(){ return std::to_string(y) + "*" + std::to_string(x) + "*" + std::to_string(t);}
		std::string toStringyx(){return std::to_string(y) + "*" + std::to_string(x);}
		int toId(int gridSize){return x + y*gridSize;}
		int toId_t(int gridSize){return x + y*gridSize + t*gridSize*gridSize;}
		int x,y,t;

	};
	typedef std::shared_ptr<R2TimeState> R2TimeStatePtr;


	struct R2Bounds{
		R2Bounds()
		{x_lower = 0;
		 y_lower = 0;
		 x_upper = 0;
		 y_lower = 0;
		 }
		R2Bounds(int y_l, int y_u, int x_l, int x_u)
		: x_lower{x_l}
		, x_upper{x_u}
		, y_lower{y_l}
		, y_upper{y_u}
		{}
		int x_lower, x_upper, y_lower, y_upper;
	};

	class R2TimeStateSpace{
	public:
		R2TimeStateSpace(){}

		void setBounds(int y_l, int y_u, int x_l, int x_u, int t){
			bounds.y_lower = y_l;
			bounds.y_upper = y_u;
			bounds.x_lower = x_l;
			bounds.x_upper = x_u;
			timeBound = t;
		}

		bool isValid(int y, int x, int t) const{
			if(x < bounds.x_lower || x >= bounds.x_upper
				|| y < bounds.y_lower || y >= bounds.y_upper || t >=timeBound || t < 0){
				return false;
			}
			return true;
		}
		
		R2Bounds getBounds(){return bounds;}

		int getManhattan(const R2TimeStatePtr& state_1, const R2TimeStatePtr& state_2) const{
			return abs(state_1.get()->x - state_2.get()->x) + abs(state_1.get()->y - state_2.get()->y);
		}


	private:
		R2Bounds bounds;
		int timeBound;
	};
	typedef std::shared_ptr<R2TimeStateSpace> R2TimeStateSpacePtr;
 
	
} //namespace mtp




#endif //MOVING_TARGET_PLANNER_INCLUDE_STATES_COMMON_H_












