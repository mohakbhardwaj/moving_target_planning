import numpy
from RRTTree import RRTTree
from random import random

class RRTPlanner(object):

    def __init__(self, planning_env):
        self.planning_env = planning_env

    def isSameConfig(self, config_1, config_2):
        if config_1[0] == config_2[0] and config_1[1] == config_2[1]:
            return True
        return False

    def Plan(self, start_config, goal_config):
        print "Planner start", start_config
        tree = RRTTree(self.planning_env, start_config)
        plan = []
        extend_config = start_config
        k = 0
        while  True:
            k = k + 1
            # print "k", k
            r = random()
            if r > self.planning_env.p:
                target_config = self.planning_env.GenerateRandomConfiguration()
            else:
                target_config = goal_config
            
            # print target_config[0]
            # print tree.GetNearestVertex(goal_config)
            current_id, current_config = tree.GetNearestVertex(target_config)
            # print current_config
            extend_config = self.planning_env.Extend(current_config, target_config)
            
            # print "Extend", extend_config
            if extend_config == None:
                pass
            else:
                # if(extend_config[0] == 992):
                # #     print extend_config
                # if(self.isSameConfig(extend_config, goal_config)):
                #     print "Aloha"
                
                y = [int(i) for i in range(1, abs(extend_config[0] - current_config[0]) + 1)]
                x = [int(i) for i in range(1, abs(extend_config[1] - current_config[1]) + 1)]

                temp = list(current_config)
                # print "temp as curr", temp
                yDir = numpy.sign(extend_config[0] - current_config[0])
                xDir = numpy.sign(extend_config[1] - current_config[1])
                #Move in y first
                k = 0
                for j in xrange(len(y)):
                    temp[0] = current_config[0] + yDir*y[j]
                    temp_id = tree.AddVertex(temp)
                    tree.AddEdge(current_id, temp_id)
                    current_id = temp_id
                    k = k + 1

                
                for j in xrange(len(x)):
                    temp[1] = current_config[1] + xDir*x[j]
                    temp_id = tree.AddVertex(temp)
                    tree.AddEdge(current_id, temp_id)
                    current_id = temp_id
                    k = k + 1
                print "Num additions", k
                # print "Extend is", extend_config
                # print "Temp is", temp

                if self.isSameConfig(extend_config, goal_config):
                    print "Broke free"
                    break
        print len(tree.vertices)
        plan = []
        while current_id != 0:
            current_id = tree.edges[current_id] 
            prev_node = tree.vertices[current_id]
            plan.insert(0,prev_node)
        
        plan.append(goal_config)

        return plan