import sys
import time
from RRTPlanner import RRTPlanner
import numpy as np
import matplotlib.pylab as plt


f = open(sys.argv[1] , 'r')

flag = "X"
state = []
trajectory = []


def comma_remover(string):
    string = string.replace(",", " ")
    string = string.split()
    string = [int(k) for k in string]
    return string


def manhattan_distance(init, target):
    return abs(init[0] - target[0]) + abs(init[1] - target[1])
for line in f:
    if line[0] == "B":
        flag = "B"
        continue
    elif line[0] == "T":
        flag = "T"
        continue
    elif line[0] == "N":
        flag = "N"
        continue
    elif line[0] == "R":
        flag = "R"
        continue
    if flag == "B":
        state.append(comma_remover(line))
    elif flag == "T":
        trajectory.append(comma_remover(line))
    elif flag == "N":
        dim = int(line[0:])
        flag = "X"
    elif flag == "R":
        initial = comma_remover(line)
        flag = "X"
f.close()

start = manhattan_distance(initial, trajectory[0]) / 2

planner = RRTPlanner(state, dim, initial)
prob = 0.1

minimum_steps = start

start = time.time()

while minimum_steps < len(trajectory):
    if manhattan_distance(initial, trajectory[minimum_steps]) < (minimum_steps + 1):
        path = 0
        print trajectory[minimum_steps], minimum_steps
        plan = planner.Plan(trajectory[minimum_steps], minimum_steps, prob)
        print plan
        prob = 0.2
        for j in range(1, len(plan)):
            path += manhattan_distance(plan[j - 1], plan[j])
        print path, minimum_steps
        if path < minimum_steps:
            plan = planner.planning_env.ShortenPath(plan, 0.5)
        else:
            minimum_steps += (path - minimum_steps) / 2
            continue
        final_plan = [[0, 0]]
        for i in range(1, len(plan)):
            y = list(np.linspace(plan[i - 1][0], plan[i][0], manhattan_distance(plan[i - 1], plan[i]) + 1, dtype=int))
            x = list(np.linspace(plan[i - 1][1], plan[i][1], manhattan_distance(plan[i - 1], plan[i]) + 1, dtype=int))
            for j in range(1, len(y)):
                if y[j - 1] != y[j]:
                    final_plan.append([y[j], final_plan[-1][1]])
                if x[j - 1] != x[j]:
                    final_plan.append([final_plan[-1][0], x[j]])
        break
    else:
        minimum_steps += 1

    if minimum_steps > len(trajectory):
        minimum_steps -= path


print "Plan", plan
print "Plan found in ", time.time() - start

cost = 0

for i in final_plan:
    cost += state[i[0]][i[1]]
    state[i[0]][i[1]] = 1500

print "Cost ", cost

f = open("solution" + ".txt", 'w')
f.writelines(str(final_plan))


matrix = np.matrix(state)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_aspect('equal')
plt.imshow(matrix, interpolation='nearest', cmap=plt.cm.ocean)
plt.colorbar()
plt.show()
