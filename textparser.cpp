include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <sstream>

std::vector<int> stringtoInt(std::string Input)
{
	int i;
	std::vector<int> Output;
	std::stringstream ss(Input);

	while (ss >> i)
	{
		Output.push_back(i);

		if (ss.peek() == ',')
			ss.ignore();
	}

	return Output;
}

int main()
{
	std::string line;
	std::ifstream myfile;
	myfile.open("problem_0.txt");
	char flag;
	std::vector<std::pair<int, int> > trajectory;
	std::vector<std::vector<int> > state;
	int dimension;
	std::pair<int, int> start;
	while (getline(myfile, line))
	{
		if (line[0] == 'B')
		{
			flag = 'B';
			continue;
		}
		else if (line[0] == 'T')
		{
			flag = 'T';
			continue;
		}
		else if (line[0] == 'N')
		{
			flag = 'N';
			continue;
		}
		else if (line[0] == 'R')
		{
			flag = 'R';
			continue;
		}

		if (flag == 'B')
		{
			state.push_back(stringtoInt(line));
		}
		else if (flag == 'T')
		{
			auto temp = stringtoInt(line);
			trajectory.push_back(std::make_pair(temp[0], temp[1]));
		}
		else if (flag == 'N')
		{
			flag = 'X';
			dimension = stoi(line);
		}
		else if (flag == 'R')
		{
			flag = 'X';
			auto temp = stringtoInt(line);
			start = std::make_pair(temp[0], temp[1]);
		}
	}
	
	/*
	std::cout << "Dimension" << dimension << std::endl;
	std::cout << "Start" << start.first << start.second << std::endl;
	for (auto iter : trajectory)
	{
		std::cout << iter.first << iter.second << std::endl;
	}
	for (auto iter : state)
	{
		for (auto iter2 : iter)
		{
			std::cout << iter2 << " ";
		}
		std::cout << std::endl;
	}
	*/
}