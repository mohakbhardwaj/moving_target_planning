#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_BACKWARD_WEIGHTED_A_STAR_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_BACKWARD_WEIGHTED_A_STAR_H_

/*Planner that searhes backwards from multiple goals to the start 
  using A* search */

//C++ Headers
#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <functional>
#include <queue>
#include <set>
#include <utility>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <chrono>
#include <unordered_map>
//Project Headers
#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"
#include "planners/search_planning_common.hpp"
#include "planners/search_costs.hpp"
#include "base_planner.hpp"


namespace mtp{


	class BackwardWeightedAstarPlanner: public BasePlanner{
	public:
		BackwardWeightedAstarPlanner(const R2TimeStateSpacePtr& s, const EnvPtr& env)
		: BasePlanner(s, env)
		, costCache(env.get()->getGridSize(), std::vector<int>(env.get()->getGridSize()))
		, costCacheInitialized{false}
		, heuristicCache_(env.get()->getGridSize(), std::vector<int>(env.get()->getGridSize()))
		{}

		virtual ~BackwardWeightedAstarPlanner(){}

		virtual bool Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr&)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost);

		//Core of the planning
		virtual bool Solve(const PlanningProblem& problem, PlanningSolution& solution);
		virtual void Refresh();	
		void getSuccessorNodes(const NodePtr& s, std::vector<NodePtr>& succNodes);
		bool isStart(const R2TimeStatePtr& state, const int start) const;
		bool isClosed(const R2TimeStatePtr& n1) const;
		void addToClosed(const NodePtr& n1);
		virtual bool getCachedCosts(std::vector<std::vector<int> >& costs){
			if(savePathCosts_ && costCacheInitialized){
				costs = costCache;
				return true;
			}
			return false;
		}
		virtual void setHeuristicCache(std::vector<std::vector<int> >& heuristicCache){

			heuristicCache_ = heuristicCache;
		}
		PlanningSolution backTrack(NodePtr& n, int numExpansions) const;
	private:
		
 		 double planTime_;
 		 bool ignore_;
 		 std::function<int(const R2TimeStatePtr&)> heuristic_;
 		 std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost_;
 		 std::priority_queue<NodePtr, std::vector<NodePtr>, NodeCmp> open; 
 		 std::unordered_set<int> closed;
 		 std::vector<std::vector<int> > costCache;
 		 bool costCacheInitialized;
 		 bool savePathCosts_;
 		 std::vector <std::vector<int> > primitives{{0,0,-1},
 		 											{0,1,-1},
													{0,-1,-1},
													{1,0,-1},
													{-1,0,-1}};
		 std::vector<std::vector<int> > heuristicCache_;

	};



} //namespace mtp



#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_BACKWARD_WEIGHTED_A_STAR_H_
