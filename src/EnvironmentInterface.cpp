//Related Headers
//C Headers
//C++ Headers
//Library Headers
//Project Headers
#include "environment_interface.hpp"
#include "states_common.hpp"


namespace mtp{
	

	// bool RepresentationInterface::ReadInputFile(){
	// 	//Reads config file and initializes environment
	// 	env_file.open(configfile);
	// 	if(!env_file.is_open(){
	// 		std::cout << "Did not find file" <<std::endl;
	// 		return false;
	// 	}
	// 	std::string line;
	// 	while(env_file){
	// 		std::getline(env_file, line);
	// 		stringstream ss(line);
	// 		std::cout << fileLine << std::endl
	// 		//[TODO: Implement]
	// 	}

	// }

	bool EnvironmentInterface::inCollision(const R2TimeStatePtr& state) const{
	//Although not required in th current problem, it is generally useful	
		if(costMap[state.get()->y][state.get()->x] == -1){
			return true;
		}
		return false;
	}
	double EnvironmentInterface::getCost(const R2TimeStatePtr& state) const{
		return costMap[state.get()->y][state.get()->x];
	}

	void EnvironmentInterface::getCostMap(std::vector<std::vector<int> >&c) const{
		c = costMap;
	}

	bool EnvironmentInterface::setCostMap(const std::vector<std::vector<int> >& c){
		//Set costMap if you had to reset 
		costMap = c;
		initialized = true;
		return true;
	}
	void EnvironmentInterface::reset(){
		//Reset the environment interface
		costMap.clear();
		initialized = false;
	}




} //namespace mtp