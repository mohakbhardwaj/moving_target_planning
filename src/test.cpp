#include <iostream>
#include "environment_interface.hpp"
#include "states_common.hpp"
#include "planning_common.hpp"
#include "planner_main.hpp"
#include "base_planner.hpp"
#include "planners/AstarPlanner.hpp"

int main(int argc, char** argv){


	std::cout << "I have arrived!!!!!" << std::endl;
	//Read File Test
	mtp::ConfigFileParser parser("../resources/problem-1.txt");
	
	std::cout << "In init bro" << std::endl;
	mtp::CompleteDefinition d = parser.ReadFile();
	mtp::EnvPtr env = std::make_shared<mtp::EnvironmentInterface>(d.costMap);
	mtp::R2TimeStateSpacePtr sspace = std::make_shared<mtp::R2TimeStateSpace>();
	std::cout << "Env and sspace done" << std::endl;
	sspace.get()->setBounds(0, d.gridSize, 0, d.gridSize, d.timeBound);
	std::cout << "Bounds set" << std::endl;
	// PrintCompleteDefinition(d);
	
	mtp::PlannerMain mainplan(env, sspace);
	std::cout << "Mainplanner created" << std::endl;
	bool mainPlanInitialized = mainplan.initialize("planner_1");
	std::cout << "Mainplanner init" << std::endl;
	if(!mainPlanInitialized){
		std::cout << mainplan.err_str();
	}
	mtp::PlanningSolution solution;
	bool planned = mainplan.Plan(d.problem, solution);
	solution.Print(true);
	std::cout << "Planned is: " << planned << std::endl;

	return 0;
}