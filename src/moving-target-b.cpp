//Main file for running optimal planner for moving target.
//Takes relative path to problem file as command line argument
//eg. ./moving-target-a ../resources/problem-0.txt

#include <iostream>
#include <fstream>
#include <string>
#include "environment_interface.hpp"
#include "states_common.hpp"
#include "planning_common.hpp"
#include "planner_main.hpp"
#include "base_planner.hpp"


int main(int argc, char** argv){

	//Read File Test
	std::string fileName;
	bool printOnScreen{false};

	if(argc > 1){

		std::string problemNumber(argv[1]);
		fileName = "../resources/" + problemNumber + ".txt";
		if(argc > 2){
			std::string arg2(argv[2]);
			if(arg2.compare("1") == 0){
				printOnScreen = true;
			}
		}	
	}
	else{
		fileName = "../resources/problem-1.txt";
		printOnScreen = false;
	}

	mtp::ConfigFileParser parser(fileName);
	
	mtp::CompleteDefinition d = parser.ReadFile();
	mtp::EnvPtr env = std::make_shared<mtp::EnvironmentInterface>(d.costMap);
	mtp::R2TimeStateSpacePtr sspace = std::make_shared<mtp::R2TimeStateSpace>();

	sspace.get()->setBounds(0, d.gridSize, 0, d.gridSize, d.timeBound);
	
	
	mtp::PlannerMain mainplan(env, sspace);
	
	bool mainPlanInitialized = mainplan.initialize("planner_2");
	
	if(!mainPlanInitialized){
		std::cout << mainplan.err_str();
	}
	mtp::PlanningSolution solution;
	bool planned = mainplan.Plan(d.problem, solution);
	//File to write output to
	std::string outputFileName = "../resources/moving-target-b-solution.txt";
	std::ofstream outputFile(outputFileName);
	;
	if(planned){
		solution.Print(printOnScreen);
		solution.writeToFile(outputFile);
	}

	std::cout << "[INFO] Stored results in resources folder\n";
	
	return 0;
}