#!/usr/bin/env python
import sys
import time
from RRTPlanner import RRTPlanner
import numpy as np
import matplotlib.pyplot as plt
from EnvironmentInterface import EnvironmentInterface

#Set the planner parameters
collision_thresh = 200

def prune_goals(start_state, goal_states, env):
	pruned_goals = []
	for i in xrange(len(goal_states)):
		if(env.ComputeDistance(start_state, goal_states[i][0]) > i):
			continue
		pruned_goals.append(goal_states[i])
	return pruned_goals



def parse_line(string):
    string = string.replace(",", " ")
    string = string.split()
    string = [int(k) for k in string]
    return string

def file_parser(f):
	start_state = []
	goal_states = []
	costmap = []
	gridSize = 0
	flag = "X"
	timestep = 0
	for line in f:
		if line[0] == "B":
			flag = "B"
			continue
		elif line[0] == "T":
			flag = "T"
			continue
		elif line[0] == "N":
			flag = "N"
			continue
		elif line[0] == "R":
			flag = "R"
			continue
		if flag == "B":
			costmap.append(parse_line(line))
		elif flag == "T":
			s = parse_line(line)
			goal_states.append([s,timestep])
			timestep = timestep + 1
		elif flag == "N":
			gridSize = int(line[0:])
			flag = "X"
		elif flag == "R":
			start_state = parse_line(line)
			flag = "X"
	return start_state, goal_states, costmap, gridSize

def main():
	f = open(sys.argv[1], 'r')
	start_state, goal_states, costmap, gridSize = file_parser(f)
	f.close()
	# print start_state
	print len(goal_states)
	env = EnvironmentInterface(gridSize, costmap, 100000)
	planner = RRTPlanner(env)
	print "Env created"

	#Prune the goals that are impossible to reach
	pruned_goals = prune_goals(start_state, goal_states, env)
	print len(pruned_goals)
	# print env.get_cost(start_state)
	print "start ekdum", start_state
	print goal_states[0][0]
	plan = planner.Plan(start_state, goal_states[0][0])

	print plan










if __name__ == "__main__":
	main()







