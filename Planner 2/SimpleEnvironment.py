import numpy as np
import random
import time
import collections

compare = lambda x, y: collections.Counter(x) == collections.Counter(y)


class SimpleEnvironment:
    
    def __init__(self, state, N):
        self.state = state
        self.boundary_limits = [[0, 0], [N, N]]
        self.lower_limits = [0, 0]
        self.upper_limits = [N, N]

    def SetGoalParameters(self, goal_config, p=0.1):
        self.goal_config = goal_config
        self.p = p

    def collision_check(self, x, y):
        if self.state[y][x] > 200:
            return True
        return False

    def GenerateRandomConfiguration(self):
        lower_limits, upper_limits = self.boundary_limits
        #
        # TODO: Generate and return a random configuration
        #
        while True:
            y = int(np.floor(random.uniform(lower_limits[0], upper_limits[0])))
            x = int(np.floor(random.uniform(lower_limits[1], upper_limits[1])))
            if self.collision_check(x, y):
                pass
            else:
                return np.array([y, x])


    def ComputeDistance(self, start_config, end_config):
        #
        # TODO: Implement a function which computes the distance between
        # two configurations
        #
        return sum(np.absolute(end_config - start_config))

    def Extend(self, start_config, end_config):
        #
        # TODO: Implement a function which attempts to extend from 
        #   a start configuration to a goal configuration
        #
        steps = (self.ComputeDistance(start_config, end_config) + 1) / 3
        y = [int(np.floor(i)) for i in np.linspace(start_config[0], end_config[0], steps)]
        x = [int(np.floor(i)) for i in np.linspace(start_config[1], end_config[1], steps)]
        config = None
        # print y, x
        for i in range(0, len(x)):
            if self.collision_check(x[i], y[i]) and i > 0:
                # print "collision", i
                return np.array([y[i - 1], x[i - 1]])
            else:
                config = np.array([y[i], x[i]])
        return config

    def ShortenPath(self, path, timeout=0.5):
        # 
        # TODO: Implement a function which performs path shortening
        #  on the given path.  Terminate the shortening after the 
        #  given timout (in seconds).
        #

        #make copy so we don't overwrite the original path
        short_path = list(path)

        init_time = time.time()
        while time.time() - init_time < timeout and len(short_path) > 2:
            start = random.randint(0, len(short_path) - 2)
            end = random.randint(start + 1, len(short_path) - 1)
            extend = self.Extend(short_path[start], short_path[end])
            if compare(extend, short_path[end]):
                short_path[start + 1:end] = []

        return short_path
