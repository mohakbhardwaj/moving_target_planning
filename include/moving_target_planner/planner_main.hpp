#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNER_MAIN_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNER_MAIN_H_

//C++ Headers
#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <set>
//Project Headers
#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"
#include "base_planner.hpp"
#include "planners/AstarPlanner.hpp"
#include "planners/Djikstra2DForwardPlanner.hpp"
#include "planners/BackwardAstarPlanner.hpp"
#include "planners/BackwardWeightedAstarPlanner.hpp"
#include "planners/search_costs.hpp"
#include "planners/search_heuristics.hpp"

namespace mtp{

	struct CompleteDefinition{
		CompleteDefinition(){}
		int gridSize;
		int timeBound;
		std::vector<std::vector<int> > costMap;
		PlanningProblem problem;
	};

	class PlannerMain{

	public:
		PlannerMain(const EnvPtr& e, const R2TimeStateSpacePtr space)
		: env(e)
		, err_str_()
		, r2_tspace_(space)
		, base_planner_a()
		, base_planner_b(){
			
			BasePlannerDatabase = {
		      {"astar_planner", BasePlannerPtr(new AstarPlanner(space, e))},
		      {"djikstra_planner", BasePlannerPtr(new Djikstra2DPlanner(space, e))},
		      {"backward_astar_planner", BasePlannerPtr(new BackwardAstarPlanner(space, e))},
		      {"backward_weighted_astar_planner", BasePlannerPtr(new BackwardWeightedAstarPlanner(space, e))}
		  };
			
		}
		//Initialize the environment, statespace etc of the main planner
		bool initialize(const std::string planner_name);
		//This is the main function that creates as baseplanner object and passes the planning problem along.
		bool Plan(const PlanningProblem& problem, PlanningSolution& solution) const;
		std::string err_str(){ return err_str_;}
	private:
		EnvPtr env;
		R2TimeStateSpacePtr r2_tspace_;
		std::string err_str_;
		BasePlannerPtr base_planner_a, base_planner_b;
		std::unordered_map<std::string, BasePlannerPtr> BasePlannerDatabase; 


	};

	class ConfigFileParser{
	public:
		ConfigFileParser(){}
		ConfigFileParser(const std::string configFile)
		: configFile_{configFile}
		{}
		CompleteDefinition ReadFile();
		std::vector<int> stringtoInt(std::string Input);
		
	private:
		std::string configFile_;
		std::ifstream config_;
		

	};

	//Utility Functions

	void PrintStatePtr(R2TimeStatePtr sptr);
	void PrintStatePtrVector(std::vector<R2TimeStatePtr> sptrVector);
	void PrintStatePtrSet(std::set<R2TimeStatePtr> sptrSet);
	void PrintPlanningProblem(const PlanningProblem p);
	void PrintCompleteDefinition(const CompleteDefinition& cd);


} //namespace mtp

#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNER_MAIN_H_