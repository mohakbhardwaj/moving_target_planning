#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_ASTAR_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_ASTAR_H_

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <functional>
#include <queue>
#include <set>
#include <unordered_set>
#include <utility>
#include <vector>
#include <algorithm>
#include <chrono>
//Project Headers
#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"
#include "planners/search_planning_common.hpp"
#include "planners/search_heuristics.hpp"
#include "planners/search_costs.hpp"
#include "base_planner.hpp"




namespace mtp{


	class AstarPlanner: public BasePlanner{
	public:
		AstarPlanner(const R2TimeStateSpacePtr& s, const EnvPtr& env)
		: BasePlanner(s, env)
		// , node_comparison_op{"g"} 
		{}

		virtual ~AstarPlanner(){}

		virtual bool Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr&)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost);
		//Core of the planning
		virtual bool Solve(const PlanningProblem& problem, PlanningSolution& solution);
		virtual void Refresh();
		//Get 4 connected successors for current implementation
		void getSuccessorNodes(const NodePtr& s, std::vector<NodePtr>& succNodes,const std::vector<R2TimeStatePtr> curr_goal_set) const;
		bool inCurrentGoalSet(const R2TimeStatePtr& state, const std::set<std::string>& curr_goal_set) const;
		bool isClosed(const NodePtr& n1) const;
		void addToClosed(const NodePtr& n1);
		void popFromGoalSet(const R2TimeStatePtr& s);

	private:
		
 		 double planTime_;
 		 bool ignore_;
 		 std::function<int(const R2TimeStatePtr& )> heuristic;
 		 std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost;
 		 // NodeCmp is the functor for weak inequality comparison for hashing nodes
 		 std::priority_queue<NodePtr, std::vector<NodePtr>, NodeCmp> open; 
 		 std::set<std::string> closed;
 		 bool savePathCosts;
	};



} //namespace mtp



#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_ASTAR_H_
