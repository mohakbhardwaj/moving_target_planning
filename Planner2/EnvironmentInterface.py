import numpy 
import matplotlib.pyplot as pl
import random
from time import time

class EnvironmentInterface(object):
    
    def __init__(self, gridSize, costmap, collision_thresh):
        self.boundary_limits = [[0., 0], [gridSize, gridSize]]
        # goal sampling probability
        self.p = 0.2
        self.costmap = costmap
        self.collision_thresh = collision_thresh


    def SetGoalParameters(self, goal_config, p = 0.2):
        self.goal_config = goal_config
        self.p = p
        
    def check_boundary(self, y, x):
        lower_limits, upper_limits = self.boundary_limits
        if y < lower_limits[0] or y >= upper_limits[0] or x < lower_limits[1] or x >= upper_limits[1]:
            return False
        return True
    
    def collision_checker(self,config):
    	y = config[0]
    	x = config[1]
        if(self.check_boundary(y,x)):
            if(self.costmap[y][x] >= self.collision_thresh):
                return True
        return False


    def GenerateRandomConfiguration(self):
        config = [0] * 2;
        lower_limits, upper_limits = self.boundary_limits
        inCollision = True
        while inCollision:
        	config[0] = int(random.uniform(lower_limits[0], upper_limits[0]))
        	config[1] = int(random.uniform(lower_limits[1], upper_limits[1]))
        	inCollision = self.collision_checker(config) 
        # print "random_config", config
        return numpy.array(config)

    def ComputeDistance(self, start_config, end_config):

        return abs(start_config[0] - end_config[0]) + abs(start_config[1] - end_config[1])

    def Extend(self, start_config, end_config):
        # print "start in extend" , start_config
    	dist = self.ComputeDistance(start_config, end_config)

    	num_steps_y = int(abs(end_config[0] - start_config[0])/3) + 1
        num_steps_x = int(abs(end_config[1] - start_config[1])/3) + 1
        
        config_to_return = None
        #Move in y first
        directiony = numpy.sign(end_config[0] - start_config[0])
    	for i in xrange(1,num_steps_y + 1):
            
            temp_config = numpy.array([start_config[0] + directiony*i, start_config[1]])
            inCollision = self.collision_checker(temp_config)
            if inCollision:
                break
            else:
                config_to_return = temp_config
        
        if config_to_return is not None:
            start_config = config_to_return
        #Move in x now
        directionx = numpy.sign(end_config[1] - start_config[1])
        for i in xrange(1, num_steps_x + 1):
            
            temp_config = numpy.array([start_config[0], start_config[1] + directionx*i])
            inCollision = self.collision_checker(temp_config)
            if inCollision:
                break
            else:
                config_to_return = temp_config

        # print "return", config_to_return

    	return config_to_return


    def ShortenPath(self, path, timeout=5.0):
        
        t = time()
        while time() - t < timeout:
            idx1 = random.randint(0,len(path)-1)
            idx2 = random.randint(idx1,len(path)-1)
            q_new = self.Extend(path[idx1], path[idx2])
            if q_new != None:
                if numpy.array_equal(q_new,path[idx2]):
                    path[idx1+1:idx2] = []
        return path


    def InitializePlot(self, goal_config):
        self.fig = pl.figure()
        lower_limits, upper_limits = self.boundary_limits
        pl.xlim([lower_limits[0], upper_limits[0]])
        pl.ylim([lower_limits[1], upper_limits[1]])
        pl.plot(goal_config[0], goal_config[1], 'gx')

        # Show all obstacles in environment
        for b in self.robot.GetEnv().GetBodies():
            if b.GetName() == self.robot.GetName():
                continue
            bb = b.ComputeAABB()
            pl.plot([bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0]],
                    [bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1]], 'r')
                    
                     
        pl.ion()
        pl.show()

    def get_cost(self, config):
        return self.costmap[config[0]][config[1]]
        
    def PlotEdge(self, sconfig, econfig):
        pl.plot([sconfig[0], econfig[0]],
                [sconfig[1], econfig[1]],
                'k.-', linewidth=2.5)
        pl.draw()