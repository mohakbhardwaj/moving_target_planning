#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_PLANNING_COMMON_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_PLANNING_COMMON_H_

#include <memory>
#include <vector>
#include <functional>
#include <string>
#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"

namespace mtp{

	
	struct Node{
		Node()
		: state()
		, parent()
		, gValue(0)
		, fValue(0)
		{}
		Node(R2TimeStatePtr s, std::shared_ptr<Node> p, int g, int f)
		: state{s}
		, parent{p}
		, gValue{g}
		, fValue{f}
		{

			parent = p;
		}
		R2TimeStatePtr state;
		std::shared_ptr<Node> parent;
		int gValue, fValue;    //Change to double if want to try Euclidean Heuristic
	};
	typedef std::shared_ptr<Node> NodePtr;

	//Functor Class which can be used to compare nodes
	class NodeCmp{
	public:
		bool operator()(const NodePtr n1, const NodePtr n2){
			return n1.get()->fValue > n2.get()->fValue;

		}
	};





} //namespace mtp



#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_PLANNING_COMMON_H_