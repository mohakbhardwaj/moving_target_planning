#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNING_COMMON_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNING_COMMON_H_
/*Generic header file defining data structures for encapsulating a planning problem*/
//C++ Headers
#include <vector>
#include <iostream>
#include <set>
#include <memory>
#include <fstream>
//Project Headers
#include "states_common.hpp"


namespace mtp{

	struct PlanningProblem{
		PlanningProblem(){};
		PlanningProblem(R2TimeStatePtr startState, std::vector<R2TimeStatePtr> goalStates)
		: start{startState}
		, goals{goalStates}
		{}
		R2TimeStatePtr start;
		std::vector<R2TimeStatePtr> goals;

	};

	struct PlanningSolution{
		PlanningSolution(){}
		std::vector<R2TimeStatePtr> path;
		int numSteps, pathCost, numExpansions;

		void Print(bool printPath){
			std::cout << "Printing Planning Solution" << std::endl;
			std::cout <<"\tNumber of Steps: " << numSteps << std::endl;
			std::cout << "\tPath Cost: " << pathCost << std::endl;
			std::cout << "\tNumber of expansions: " << numExpansions << std::endl;
			if(printPath){
				std::cout << "\tSolution Path : (in row,col,time format)\n";
				for(auto iter:path){
					std::cout << "\t[ " << iter.get()->y << ", " << iter.get()->x << ", " << iter.get()->t << " ]" << std::endl;
				}
			}
		}

		void writeToFile(std::ofstream &of){
			of << "\tNumber of Steps: " << numSteps << std::endl;
			of << "\tPath Cost: " << pathCost << std::endl;
			of << "\tNumber of expansions: " << numExpansions << std::endl;
			of << "Solution Path: (in row,col,time format)\n";
			for(auto iter: path){
				of << "\t[ " << iter.get()->y << ", " << iter.get()->x << ", " << iter.get()->t << " ]\n";
			}
		}


	};




	

} //namespace mtp





#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNING_COMMON_H_