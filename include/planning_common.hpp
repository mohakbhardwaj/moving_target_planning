#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNING_COMMON_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNING_COMMON_H_

//C++ Headers
#include <vector>
//Project Headers
#include "states_common.hpp"

namespace mtp{

	namespace planning_common{

		struct PlanningProblem{
			PlanningProblem(R2TimeState startState, std::vector<R2TimeState> goalStates)
			: start{startState}
			, goals{goalStates}
			{}

			R2TimeState start;
			std::vector<R2TimeState> goals;

		};
		struct PlanningSolution{
			PlanningSolution(){}
			std::vector<R2TimeState> path;
		};


	} //namespace planning_common

} //namespace mtp





#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNING_COMMON_H_