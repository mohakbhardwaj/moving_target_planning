#include "planners/AstarPlanner.hpp"


namespace mtp{


	bool AstarPlanner::Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr&)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost){

		planTime_ = planTime_;
		ignore_ = ignore;
		heuristic = heuristic;
		cost = cost;
		savePathCosts = savePathCosts;
	}

	void AstarPlanner::Refresh(){
		open = std::priority_queue<NodePtr, std::vector<NodePtr>, NodeCmp>();
		closed.clear();
	}


	void AstarPlanner::getSuccessorNodes(const NodePtr& s, std::vector<NodePtr>& succNodes, const std::vector<R2TimeStatePtr> curr_goal_set) const{

		//Time is a part of motion primitives for moving target
		//Problem specifies four connected grid only
		std::vector <std::vector<int> > primitives{ {0,0,1},
													{0,1,1},
													{0,-1,1},
													{1,0,1},
													{-1,0,1} };

		R2TimeStatePtr curr_state = s.get()->state;
		
		for(auto primitive : primitives){

			int y = curr_state.get()->y + primitive[0];
			int x = curr_state.get()->x + primitive[1];
			int t = curr_state.get()->t + primitive[2];
			if(r2_tspace_.get()->isValid(y, x, t)){
				R2TimeStatePtr succ_state = std::make_shared<R2TimeState>(y, x, t);
				int succ_gVal = s.get()->gValue + cost(succ_state, env_);
				// int succ_h = heuristic(succ_state, curr_goal_set, r2_tspace_, env_);
				int succ_h = 0;
				NodePtr succ_node = std::make_shared<Node>(succ_state, s, succ_gVal, succ_gVal + succ_h);
				if(isClosed(succ_node) || succ_node.get()->state.get()->t >= 2200){
					continue;
				}
				succNodes.push_back(succ_node);		
			}
		}
	}

	bool AstarPlanner::inCurrentGoalSet(const R2TimeStatePtr& state, const std::set<std::string>& curr_goal_set) const{

		if(curr_goal_set.count(state.get()->toString()) == 0){
			return false;
		}
		return true;
	}

	bool AstarPlanner::isClosed(const NodePtr& n1) const{

		if(closed.count(n1.get()->state.get()->toString()) == 0){
			return false;
		}
		return true;
	}

	void AstarPlanner::addToClosed(const NodePtr& n1){
		closed.insert(n1.get()->state.get()->toString());
	}
	
	bool AstarPlanner::Solve(const PlanningProblem& problem, PlanningSolution& solution){
		std::cout << "Inside Astar Solve" << std::endl;
		//Set the start Node
		std::vector<R2TimeStatePtr> curr_goal_set = problem.goals;
		std::set<std::string> goal_set;
		for(auto iter: curr_goal_set){
			goal_set.insert(iter.get()->toString());
		}

		//Parent of start must be NULL
		NodePtr startParent;
		NodePtr start = std::make_shared<Node>(problem.start, startParent, cost(problem.start,env_), cost(problem.start,env_) + 0);// + heuristic(problem.start, curr_goal_set, r2_tspace_, env_));

		open.push(start);

		
		int i = 0;
		while(!open.empty()){

		if(open.size() > 500000){ //[TODO:Experiment with this and planning time]
			err_str_ = "Timeout";
			break;
		}
		
		NodePtr curr_node = std::make_shared<Node>();
		curr_node = open.top();
		open.pop();
		// std::cout << "Num pops" << ++i << std::endl;
		if(isClosed(curr_node)){
			continue;
		}
		addToClosed(curr_node);
		std::cout << curr_node.get()->state.get()->y << " " << curr_node.get()->state.get()->x << " " << curr_node.get()->state.get()->t << " " << curr_node.get()->fValue << std::endl;
		if(inCurrentGoalSet(curr_node.get()->state, goal_set)){
			std::cout << "Found Plan" << std::endl;
			std::cout << "Reached Goal: " << curr_node.get()->state.get()->y << " " << curr_node.get()->state.get()->x << " " << curr_node.get()->state.get()->t << std::endl;
			break;
		}
		std::vector<NodePtr> succNodes;
		getSuccessorNodes(curr_node, succNodes, curr_goal_set);

		for(auto iter: succNodes){
			// std::cout << "Opn size " << open.size() << std::endl;
			open.push(iter);
		}   //[TODO: Use for_each]


	}
	
	return true;
	}
	


} //namespace mtp