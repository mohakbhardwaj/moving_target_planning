#ifndef MOVING_TARGET_PLANNER_INCLUDE_BASE_PLANNER_H_
#define MOVING_TARGET_PLANNER_INCLUDE_BASE_PLANNER_H_

//C++ headers
#include <iostream>
#include <memory>
#include <string>
#include <functional>
//Project Headers
#include "states_common.hpp"
#include "environment_interface.hpp"
#include "planning_common.hpp"

namespace mtp{

	class BasePlanner{

	public:
		BasePlanner()
		: env_{}
		, r2_tspace_{}
		{

		}
		BasePlanner(const R2TimeStateSpacePtr& s, const EnvPtr& env)
		: r2_tspace_{s}
		, env_{env}
		, gridSize{env.get()->getGridSize()}
		{
			env.get()->getCostMap(costMap);
		}
		virtual ~BasePlanner(){}
		//Reads parameters and sets up internal cost functions that don't change 
		//in every cycle
		virtual bool Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr& s)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost){
			return true;
		}

		//Core of the planning
		virtual bool Solve(const PlanningProblem& problem, PlanningSolution& solution) = 0;
		
		virtual void Refresh(){
			
		}
		virtual bool getCachedCosts(std::vector<std::vector<int> >& costs){return true;}
		virtual void setHeuristicCache(std::vector<std::vector<int> >& heuristicCache){}

		//Accessor
		void setEnvironment(const EnvPtr& e){ env_ = e;}
		void setStateSpace(const R2TimeStateSpacePtr& s){r2_tspace_ = s;}
		std::string err_str(){
			return err_str_;
		}
		std::vector<R2TimeStatePtr> PruneGoals(const R2TimeStatePtr& start, const std::vector<R2TimeStatePtr>& goal_set) const{
		//This function prunes out goals that will never be reachable.
		//Based on Manhattan distance of goal from start which should be 
		//less than the time it takes for the target to reach the goal for it to 
		//even be considered viable
		std::vector<R2TimeStatePtr> pruned_goals;
		for(auto iter: goal_set){
			int timeToGoal = iter.get()->t;
			int manhattanToStart  = r2_tspace_.get()->getManhattan(start, iter);
			if(timeToGoal >= manhattanToStart){
				pruned_goals.push_back(iter);
			}
		}
		return pruned_goals;
	}

	protected:

		R2TimeStateSpacePtr r2_tspace_;
		EnvPtr env_;
		std::string err_str_;
		std::vector<std::vector<int> > costMap;
		int gridSize;
	};
		

	typedef std::shared_ptr<BasePlanner> BasePlannerPtr;



} //namespace mtp



#endif //MOVING_TARGET_PLANNER_INCLUDE_BASE_PLANNER_H_