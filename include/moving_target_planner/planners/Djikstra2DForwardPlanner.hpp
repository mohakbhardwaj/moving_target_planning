#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_DJIKSTRA_2D_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_DJIKSTRA_2D_H_

//This planner is used in forward direction to calculate informed heuristics. The planner
//extends like Djikstra from a start location and fills the entire space. The values are saved in 
//a table and can be read to be reused

//C++ Headers
#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <functional>
#include <queue>
#include <set>
#include <utility>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <chrono>
#include <unordered_map>
//Project Headers
#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"
#include "planners/search_planning_common.hpp"
#include "planners/search_costs.hpp"
#include "base_planner.hpp"




namespace mtp{


	class Djikstra2DPlanner: public BasePlanner{
	public:
		Djikstra2DPlanner(const R2TimeStateSpacePtr& s, const EnvPtr& env)
		: BasePlanner(s, env)
		, costCache(env.get()->getGridSize(), std::vector<int>(env.get()->getGridSize()))
		, costCacheInitialized{false}
		{}

		virtual ~Djikstra2DPlanner(){}

		virtual bool Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr& s)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost);

		//Core of the planning
		virtual bool Solve(const PlanningProblem& problem, PlanningSolution& solution);
		virtual void Refresh();	
		void getSuccessorNodes(const NodePtr& s, std::vector<NodePtr>& succNodes);
		bool inCurrentGoalSet(const R2TimeStatePtr& state, const std::unordered_set<int>& curr_goal_set) const;
		bool isClosed(const R2TimeStatePtr& n1) const;
		void addToClosed(const NodePtr& n1);
		void popFromGoalSet(const R2TimeStatePtr& s, std::unordered_set<int>& curr_goal_set);
		virtual bool getCachedCosts(std::vector<std::vector<int> >& costs){
			if(savePathCosts_ && costCacheInitialized){
				costs = costCache;
				return true;
			}
			return false;
		}
	private:
		// std::function<
 		 double planTime_;
 		 bool ignore_;
 		 std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost_;
 		 // NodeCmp node_comparison_op;
 		 std::priority_queue<NodePtr, std::vector<NodePtr>, NodeCmp> open; 
 		 std::unordered_set<int> closed;
 		 std::vector<std::vector<int> > costCache;
 		 bool costCacheInitialized;
 		 bool savePathCosts_;
 		 std::vector <std::vector<int> > primitives{ {0,1},
													{0,-1},
													{1,0},
													{-1,0}};
		



     };


} //namespace mtp



#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_DJIKSTRA_2D_H_
