cmake_minimum_required(VERSION 2.8.3)
project(moving_target_planner)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(include/moving_target_planner)


## Declare a C++ library
add_library(MovingTargetPlanner
   src/environment_interface.cpp
   src/planners/search_costs_heuristics.cpp
   src/planners/AstarPlanner.cpp
   src/planners/Djikstra2DForwardPlanner.cpp
   src/planners/BackwardAstarPlanner.cpp
   src/planners/BackwardWeightedAstarPlanner.cpp
   src/planner_main.cpp
)


## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(compressor_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
add_executable(moving-target-a src/moving-target-a.cpp)
target_link_libraries(moving-target-a MovingTargetPlanner)

add_executable(moving-target-b src/moving-target-b.cpp)
target_link_libraries(moving-target-b MovingTargetPlanner)



