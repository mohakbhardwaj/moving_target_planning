#ifndef MOVING_TARGET_PLANNER_INCLUDE_ENVIRONMENT_INTERFACE_H_
#define MOVING_TARGET_PLANNER_INCLUDE_ENVIRONMENT_INTERFACE_H_
//Related Headers
//C Headers
//C++ Headers
#include <iostream>
#include <vector>
#include <sstream>
#include <memory>
#include <fstream>
#include <exception>
//Other libraries
//Project Headers
#include "planning_common.hpp"
#include "states_common.hpp"

namespace mtp{



	class EnvironmentInterface{
	public:
		EnvironmentInterface(){}
		EnvironmentInterface(const std::vector<std::vector<int> > &c)
		:costMap{c}{
			initialized = true;
		}
		bool inCollision(const R2TimeStatePtr& state) const;
		double getCost(const R2TimeStatePtr& state) const;
		void getCostMap(std::vector<std::vector<int> >& c) const;
		bool setCostMap(const std::vector<std::vector<int> >& c);
		int getGridSize(){return costMap.size();}
		void reset();
	private:
		std::vector<std::vector<int> > costMap;
		bool initialized;
		
	};
	typedef std::shared_ptr<EnvironmentInterface> EnvPtr;


} //namespace mtp

#endif //MOVING_TARGET_PLANNER_INCLUDE_ENVIRONMENT_INTERFACE_H_