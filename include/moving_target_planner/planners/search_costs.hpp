#ifndef MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_COSTS_H_
#define MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_COSTS_H_

#include <memory>
#include <vector>

#include "states_common.hpp"
#include "planning_common.hpp"
#include "environment_interface.hpp"
#include "planners/search_planning_common.hpp"


namespace mtp{


	int GridCost(const R2TimeStatePtr& s, const EnvPtr& env);

	int GridAndTimeCost(const R2TimeStatePtr& s, const EnvPtr& env);

} //namespace mtp

#endif //MOVING_TARGET_PLANNER_INCLUDE_PLANNERS_SEARCH_COSTS_H_