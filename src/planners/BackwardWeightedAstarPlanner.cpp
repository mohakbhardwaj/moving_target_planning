#include "planners/BackwardWeightedAstarPlanner.hpp"

namespace mtp{
	bool BackwardWeightedAstarPlanner::Initialize(double planTime, bool ignore, bool savePathCosts, std::function<int(const R2TimeStatePtr&)> heuristic, std::function<int(const R2TimeStatePtr& , const EnvPtr&)> cost){
	planTime_ = planTime_;
	ignore_ = ignore;
	cost_ = cost;
	heuristic_ = heuristic;
	savePathCosts_ = savePathCosts;
	}

	void BackwardWeightedAstarPlanner::Refresh(){
		open = std::priority_queue<NodePtr, std::vector<NodePtr>, NodeCmp>();
		closed.clear();
	}

	void BackwardWeightedAstarPlanner::getSuccessorNodes(const NodePtr& s, std::vector<NodePtr>& succNodes){

		//Problem specifies four connected grid only

		R2TimeStatePtr curr_state = s.get()->state;
		
		for(auto primitive : primitives){

			int y = curr_state.get()->y + primitive[0];
			int x = curr_state.get()->x + primitive[1];
			int t = curr_state.get()->t + primitive[2];
			if(r2_tspace_.get()->isValid(y, x, t)){
				R2TimeStatePtr succ_state = std::make_shared<R2TimeState>(y, x, t);
				int succ_gVal = s.get()->gValue + costMap[y][x];	
				if(isClosed(succ_state)){
					continue;
				}
				else{
					NodePtr succ_node = std::make_shared<Node>(succ_state, s, succ_gVal, succ_gVal + 3*heuristicCache_[y][x] - costMap[y][x]);
					succNodes.push_back(succ_node);
					}
				}
						
				}
			}

	bool BackwardWeightedAstarPlanner::isStart(const R2TimeStatePtr& state, const int start) const{

		if(state.get()->toId_t(gridSize) == start){
			return true;
		}
		return false;
	}

	bool BackwardWeightedAstarPlanner::isClosed(const R2TimeStatePtr& n1) const{

		if(closed.count(n1.get()->toId_t(gridSize)) == 0){
			return false;
		}
		return true;
	}

	void BackwardWeightedAstarPlanner::addToClosed(const NodePtr& n1){
		closed.insert(n1.get()->state.get()->toId_t(gridSize));
	}

	
	bool BackwardWeightedAstarPlanner::Solve(const PlanningProblem& problem, PlanningSolution& solution){
		
		auto start_time = std::chrono::high_resolution_clock::now();
		//Set the start Node
		std::vector<R2TimeStatePtr> curr_goal_set = problem.goals;
		std::vector<R2TimeStatePtr> pruned_goal_set = PruneGoals(problem.start, curr_goal_set);
		int startId = problem.start.get()->toId_t(gridSize);

		NodePtr goalParent;
		for(auto iter: pruned_goal_set){
			NodePtr goal = std::make_shared<Node>(iter, goalParent, costMap[iter.get()->y][iter.get()->x], heuristicCache_[iter.get()->y][iter.get()->x]); //costMap[iter.get()->y][iter.get()->x]
			open.push(goal);
		}

		int i = 0;
		
		std::vector<NodePtr> succNodes;
		NodePtr curr_node = std::make_shared<Node>();
		while(!open.empty()){
			++i;
		if(open.size() > 5000000){ 
			err_str_ = "Timeout";
			break;
		}		
		curr_node = open.top();
		open.pop();

		if(savePathCosts_){
			int r = curr_node.get()->state.get()->y;
			int c = curr_node.get()->state.get()->x;
			int costVal = curr_node.get()->gValue;
			costCache[r][c] = costVal;
		}

		if(isStart(curr_node.get()->state, startId)){
			
			solution = backTrack(curr_node, i);
			break;
		}
		if(isClosed(curr_node.get()->state)){
			continue;
		}
		//When node is popped, we have found smallest path to it. Add it to closed
		addToClosed(curr_node);
		succNodes.clear();
		//Generate valid successors
		getSuccessorNodes(curr_node, succNodes);
		for(auto iter: succNodes){
			open.push(iter);
		}   
	}
	if(savePathCosts_){
		costCacheInitialized = true;
	}
	
	auto end_time = std::chrono::high_resolution_clock::now();
	std::cout << "[INFO] Weighted Backward Planner took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << " milliseconds\n";
	return true;
	}
	
	PlanningSolution BackwardWeightedAstarPlanner::backTrack(NodePtr& n, int numExpansions) const{
		PlanningSolution solution;
		solution.pathCost = n.get()->parent.get()->gValue;
		solution.numExpansions = numExpansions;
		while(n.get()->parent){
			solution.path.push_back(n.get()->state);
			n = n.get()->parent;
		}
		//Push the last state in the path
		solution.path.push_back(n.get()->state);
		solution.numSteps = solution.path.size();
		return solution; 

	}


} //namespace mtp